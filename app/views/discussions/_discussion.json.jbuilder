json.extract! discussion, :id, :username, :titulo, :comment, :created_at, :updated_at
json.url discussion_url(discussion, format: :json)
