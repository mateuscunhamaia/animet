class CreateDiscussions < ActiveRecord::Migration[6.0]
  def change
    create_table :discussions do |t|
      t.string :username
      t.string :titulo
      t.text :comment

      t.timestamps
    end
  end
end
